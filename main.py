# %%
import cv2
import time
import numpy as np
import glob
import os
from BackgroundRemover import BackgroundRemover
from FaceDetector import FaceDetector
from SkinDetector import SkinDetector


# %%
def computeMatchScores(source, images):
    scores = []
    for image in images:
        result = cv2.matchTemplate(source, image, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
        scores.append(max_val)
    return scores


def removeSmallContours(contours, fore):
    for c in contours:
        if cv2.contourArea(c) < 15000:
            x, y, w, h = cv2.boundingRect(c)
            cv2.rectangle(fore, (x, y), (x + w, y + h), (0, 0, 0), -1)


def crop_minAreaRect(img, rect):

    # rotate img
    angle = rect[2]
    rows, cols = img.shape[0], img.shape[1]
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
    img_rot = cv2.warpAffine(img, M, (cols, rows))

    # rotate bounding box
    rect0 = (rect[0], rect[1], 0.0)
    box = cv2.boxPoints(rect)
    pts = np.int0(cv2.transform(np.array([box]), M))[0]
    pts[pts < 0] = 0

    # crop
    img_crop = img_rot[pts[1][1]:pts[0][1],
                       pts[1][0]:pts[2][0]]

    return img_crop

# %%
size = (128, 128)
templatesPath = 'templates/'
filePaths = glob.glob(templatesPath + '*.jpg')
templates = []
templateLabels = []
for path in filePaths:
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    image = cv2.resize(image, size)
    label = os.path.basename(path).split('.')[0]
    templates.append(image)
    templateLabels.append(label)


# %%
video = cv2.VideoCapture(0)
success, frame = video.read()
faceDetector = FaceDetector()
skinDetector = SkinDetector()
backgroundRemover = BackgroundRemover()
backgroundRemover.calibrate(frame)
fps = video.get(cv2.CAP_PROP_FPS)
size = (128, 128)
count = 1
kernel = np.ones((5, 5), np.uint8)
handMask = np.zeros((300, 300))
contours = None


# %%
while success:
    faces = faceDetector.detectFaces(frame, equalize=True)
    if faces.size > 0:
        face = faces[0]
        x, y, w, h = face
        skinDetector.calibrate(frame[y:y + h, x:x + w])
    foreground = skinDetector.getSkinMask(frame)
    foreground = faceDetector.removeFaces(foreground, faces)
    image, contours, hierarchy = cv2.findContours(foreground, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    if contours:
        removeSmallContours(contours, foreground)
        cv2.imshow('base', foreground)
        c = max(contours, key=cv2.contourArea)
        if cv2.contourArea(c) > 15000:
            x, y, w, h = cv2.boundingRect(c)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            handMask = foreground[y:y + h, x:x + w]
            handMask = cv2.resize(handMask, size)
            scores = computeMatchScores(handMask, templates)
            presentGestureIndex = np.argmax(scores)
            cv2.putText(frame, 'gesture : ' + templateLabels[presentGestureIndex], (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 0, 0), 2)

    cv2.imshow('foreground', frame)
    cv2.imshow('mask', handMask)
    key = cv2.waitKey(1)
    if key == 27:
        break
    success, frame = video.read()
    count += 1
video.release()
cv2.destroyAllWindows()
