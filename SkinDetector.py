import numpy as np
import cv2


class SkinDetector:

    def __init__(self):
        self.yLowThreshold, self.yHighThreshold = -1, 295
        self.crLowThreshold, self.crHighThreshold = 295, -1
        self.cbLowThreshold, self.cbHighThreshold = 295, -1
        self.calibrated = False

    def calibrate(self, face):
        if not self.calibrated:
            self.calculateThresholds(face)
            self.calibrated = True

    def getSkinMask(self, image):
        if not self.calibrated:
            self.applyDefaultSkinThresholds()

        lo_skin_ycrcb = (self.yLowThreshold, self.crLowThreshold, self.cbLowThreshold)
        hi_skin_ycrcb = (self.yHighThreshold, self.crHighThreshold, self.cbHighThreshold)
        ycrcbImage = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)
        skinMaskYcrcb = cv2.inRange(ycrcbImage, lo_skin_ycrcb, hi_skin_ycrcb)
        skinMaskYcrcb = self.post_process(skinMaskYcrcb)
        # skinMask = cv2.morphologyEx(skinMask, cv2.MORPH_OPEN, kernel)
        skinMaskYcrcb = cv2.dilate(skinMaskYcrcb, np.ones((3, 3)), iterations=1)

        return skinMaskYcrcb

    def post_process(self, mask):
        maskCopy = mask.copy()
        kernelErode = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        kernelDilate = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (6, 6))
        maskCopy = cv2.erode(maskCopy, kernelErode)
        maskCopy = cv2.dilate(maskCopy, kernelDilate)

        return maskCopy

    def calculateThresholds(self, face):
        faceYCrCb = cv2.cvtColor(face, cv2.COLOR_BGR2YCrCb)
        rows, cols, _ = face.shape
        for i in range(rows):
            for j in range(cols):
                b = face[i, j, 0]
                g = face[i, j, 1]
                r = face[i, j, 2]
                y = faceYCrCb[i, j, 0]
                cr = faceYCrCb[i, j, 1]
                cb = faceYCrCb[i, j, 2]
                gray = int(0.2989 * r + 0.5870 * g + 0.1140 * b)
                if gray < 200 and gray > 40 and r > g and r > b:
                    self.yLowThreshold = int(y if y < self.yLowThreshold else self.yLowThreshold)
                    self.yHighThreshold = int(y if y > self.yHighThreshold else self.yHighThreshold)

                    self.crLowThreshold = int(cr if cr < self.crLowThreshold else self.crLowThreshold)
                    self.crHighThreshold = int(cr if cr > self.crHighThreshold else self.crHighThreshold)

                    self.cbLowThreshold = int(cb if cb < self.cbLowThreshold else self.cbLowThreshold)
                    self.cbHighThreshold = int(cb if cb > self.cbHighThreshold else self.cbHighThreshold)

    def applyDefaultSkinThresholds(self):
        self.yLowThreshold, self.yHighThreshold = 0, 255
        self.crLowThreshold, self.crHighThreshold = 135, 180
        self.cbLowThreshold, self.cbHighThreshold = 85, 135

    def calculateThresholdsVect(self, face):
        faceYCrCb = cv2.cvtColor(face, cv2.COLOR_BGR2YCrCb)
        faceGray = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
        B = face[:, :, 0]
        G = face[:, :, 1]
        R = face[:, :, 2]
        Y = faceYCrCb[:, :, 0]
        Cr = faceYCrCb[:, :, 1]
        Cb = faceYCrCb[:, :, 2]
        boolMask = (faceGray < 200) & (faceGray > 40) & (R > G) & (R > B)
        boolMask = boolMask.astype(np.uint8)
        Y = boolMask * Y
        Cr = boolMask * Cr
        Cb = boolMask * Cb

        yLo = np.amin(Y)
        yHi = np.amax(Y)
        crLo = np.amin(Cr)
        crHi = np.amax(Cr)
        cbLo = np.amin(Cb)
        cbHi = np.amax(Cb)

        self.yLowThreshold = int(yLo)
        self.yHighThreshold = int(yHi)

        self.crLowThreshold = int(crLo)
        self.crHighThreshold = int(crHi)

        self.cbLowThreshold = int(cbLo)
        self.cbHighThreshold = int(cbHi)
