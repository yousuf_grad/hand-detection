import cv2
import numpy as np

FACE_CLASSIFIER_FILENAME = 'haar_files/haarcascade_frontalface_default.xml'


class FaceDetector:

    def __init__(self, faceClassifierFileName=FACE_CLASSIFIER_FILENAME):
        self.faceCascadeClassifier = cv2.CascadeClassifier()
        if not self.faceCascadeClassifier.load(faceClassifierFileName):
            raise RuntimeError('cannot load file ' + faceClassifierFileName)

    def removeFaces(self, output, faces):
        """ can break the code if face is detected
        near the upper edge of window, and there is
        no room for relaxing the bounding box
        """
        for (x, y, w, h) in faces:
            y = y - 50
            h = h + 150
            cv2.rectangle(output, (x, y), (x + w, y + h), (0, 0, 0), -1)
        return output

    def detectFaces(self, image, equalize=True):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray) if equalize else gray
        rows, cols = gray.shape
        scaleFactor = 1.6
        minNeighbors = 2
        flags = 0
        minSize = (int(rows / 3), int(rows / 3))
        maxSize = (int(rows * (4 / 5)), int(rows * (4 / 5)))

        faces = self.faceCascadeClassifier.detectMultiScale(
            gray, scaleFactor, minNeighbors,
            flags, minSize, maxSize
        )
        return np.array(faces)
