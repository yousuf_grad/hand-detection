import numpy as np
import cv2


class BackgroundRemover:

    def __init__(self):
        self.background = None
        self.calibrated = False

    def getForeground(self, image):
        foregroundMask = self.getForegroundMask(image)
        return foregroundMask.copy()

    def calibrate(self, image):
        self.background = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        self.calibrated = True

    def getForegroundMask(self, image):
        if not self.calibrated:
            foregroundMask = np.zeros(image.shape, np.uint8)
            return foregroundMask

        foregroundMask = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        foregroundMask = self.removeBackground(foregroundMask, self.background)
        return foregroundMask

    def removeBackground(self, image, background):
        thresholdOffset = 10

        # old implementation of background removal using nested loops
        def removeBackgroundOld(image, background):
            rows, cols = image.shape
            foregroundMask = np.zeros((rows, cols), np.uint8)

            for i in range(rows):
                for j in range(cols):
                    curr_pix = image[i, j]
                    bg_pix = background[i, j]
                    if (
                            curr_pix >= bg_pix - thresholdOffset and
                            curr_pix <= bg_pix + thresholdOffset
                    ):
                        foregroundMask[i, j] = 0
                    else:
                        foregroundMask[i, j] = 255

        bg_low = background - thresholdOffset
        bg_high = background + thresholdOffset
        foregroundMask = (image >= bg_low) & (image <= bg_high)
        foregroundMask = foregroundMask.astype(np.uint8)
        foregroundMask[foregroundMask == 0] = 255
        foregroundMask[foregroundMask == 1] = 0
        return foregroundMask
